package web.filters;
import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import java.io.IOException;


@WebFilter("/address.jsp")
public class PersonDataInSessionFilter implements Filter {
    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        String firstName = request.getParameter("firstName");
        String surname = request.getParameter("surname");
        String pesel = request.getParameter("pesel");
        if (firstName == null || surname == null || pesel == null || "".equals(firstName) || "".equals(surname) || "".equals(pesel)){
            request.setCharacterEncoding("UTF-8");
            request.setAttribute("errMsg", "Proszę uzupełnić dane");
            RequestDispatcher rd = request.getRequestDispatcher("person.jsp");
            rd.include(request, response);
        } else {
            chain.doFilter(request, response);
        }
    }

	@Override
	public void destroy() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void init(FilterConfig arg0) throws ServletException {
		// TODO Auto-generated method stub
		
	}

}