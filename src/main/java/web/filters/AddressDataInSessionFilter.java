package web.filters;
import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import java.io.IOException;

@WebFilter("/success.jsp")
public class AddressDataInSessionFilter implements Filter {

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        String city = servletRequest.getParameter("city");
        String zipCode = servletRequest.getParameter("zipCode");
        String street = servletRequest.getParameter("street");
        String hauseNumber = servletRequest.getParameter("hauseNumber");
        String localNumber = servletRequest.getParameter("localNumber");
        String phoneNumber = servletRequest.getParameter("phoneNumber");

        if(city == null || zipCode == null || street == null || hauseNumber == null || localNumber == null || phoneNumber == null
                || "".equals(zipCode) || "".equals(street) || "".equals(hauseNumber) || "".equals(localNumber) || "".equals(phoneNumber)){
            servletRequest.setCharacterEncoding("UTF-8");
            servletRequest.setAttribute("errMsg", "Proszę uzupełnić dane");
            RequestDispatcher rd = servletRequest.getRequestDispatcher("address.jsp");
            rd.include(servletRequest, servletResponse);
        } else {
            filterChain.doFilter(servletRequest, servletResponse);
        }
    }

	@Override
	public void destroy() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void init(FilterConfig arg0) throws ServletException {
		// TODO Auto-generated method stub
		
	}

}
