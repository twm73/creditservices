package web.filters;
import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import java.io.IOException;

@WebFilter("/person.jsp")
public class LoanDataInSessionFilter implements Filter {


    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain)throws IOException, ServletException {

        String amount = servletRequest.getParameter("amount");
        String installmentCount = servletRequest.getParameter("installmentCount");
        if(amount == null || "".equals(amount) || amount.equals("0")
        || installmentCount == null || "".equals(installmentCount) || installmentCount.equals("0")){
            servletRequest.setCharacterEncoding("UTF-8");
            servletRequest.setAttribute("errMsg", "Proszę uzupełnić dane");
            RequestDispatcher rd = servletRequest.getRequestDispatcher("loanParameters.jsp");
            rd.include(servletRequest, servletResponse);
        } else {
            filterChain.doFilter(servletRequest, servletResponse);
        }
    }

    @Override
    public void destroy() {

    }

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }
}
