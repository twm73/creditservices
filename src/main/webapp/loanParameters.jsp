<%@ page import="java.util.Random" %>
<%@ page import="domain.LoanApplication" %>
<%@ page import="java.util.Date" %>
<%@page language="java" contentType="text/html; ISO-8859-1" pageEncoding="UTF-8" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
    <title>Insert title here</title>
</head>
<body>
<%!
    private Random generator = new Random();
%>
<%
    String number = "" + Math.abs(generator.nextInt()) ;
%>
<jsp:useBean id="loan" class="domain.LoanApplication" scope="session"/>
<jsp:setProperty name="loan" property="number" value="<%=number %>"/>
<jsp:setProperty name="loan" property="date" value="<%=new Date()%>"/>
<p>Wygenerowany numer wniosku: <%=number%></p>
<p>Data wygenerowania: <%=loan.getDate()%></p>
<form action="person.jsp">
    <label>Wnioskowana kwota: <input type="number" id="amount" name="amount"></label><br/>
    <label>Ilość rat: <input type="number" id="installmentCount" name="installmentCount"></label><br/>
    <input type="submit" value="następny krok">
</form>
</body>
</html>